package com.space.android.rxjava2.utils;

/**
 * Created by Ordosbxy on 30/08/16.
 */
public final class AppConstant {

    private AppConstant() {
        // This class in not publicly instantiable.
    }

    public static final String LINE_SEPARATOR = "\n";

}
