package com.space.android.rxjava2.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.space.android.rxjava2.SpaceApplication;
import com.space.android.rxjava2.R;
import com.space.android.rxjava2.ui.compose.ComposeOperatorExampleActivity;
import com.space.android.rxjava2.ui.networking.NetworkingActivity;
import com.space.android.rxjava2.ui.pagination.PaginationActivity;
import com.space.android.rxjava2.ui.rxbus.RxBusActivity;

public class SelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
    }

    // 操作符Demo
    public void startOperatorsActivity(View view) {
        startActivity(new Intent(SelectionActivity.this, OperatorsActivity.class));
    }

    // 网络请求Demo
    public void startNetworkingActivity(View view) {
        startActivity(new Intent(SelectionActivity.this, NetworkingActivity.class));
    }

    // RxBus Demo
    public void startRxBusActivity(View view) {
        ((SpaceApplication) getApplication()).sendAutoEvent();
        startActivity(new Intent(SelectionActivity.this, RxBusActivity.class));
    }

    // 分页Demo
    public void startPaginationActivity(View view) {
        startActivity(new Intent(SelectionActivity.this, PaginationActivity.class));
    }

    // 自定义操作符Demo
    public void startComposeOperator(View view) {
        startActivity(new Intent(SelectionActivity.this, ComposeOperatorExampleActivity.class));
    }
}
