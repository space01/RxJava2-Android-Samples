# RxJava2 Android 示例
 1. 学习RxJava2的用法及其操作符的使用
 2. 实践响应式编程思想（ReactiveX）

 ![Rxjava2](/assets/rxjava2.png)

## 下载
 [<img src="./assets/ic_launcher_rxjava2.png" width = "96" height = "96"/>](/assets/app-debug.apk)

## 已完成操作符
 - `Map` : transform the items emitted by an Observable by applying a function to each item
 - `Zip` : combine the emissions of multiple Observables together via a specified function and emit single items for each combination based on the results of this function
 - `Filter` : emit only those items from an Observable that pass a predicate test
 - `FlatMap` : transform the items emitted by an Observable into Observables, then flatten the emissions from those into a single Observable

## 待完成操作符
 - `Take` : emit only the first n items emitted by an Observable
 - `Reduce` : apply a function to each item emitted by an Observable, sequentially, and emit the final value
 - `Skip` : suppress the first n items emitted by an Observable
 - `Buffer` : periodically gather items emitted by an Observable into bundles and emit these bundles rather than emitting the items one at a time
 - `Concat` : emit the emissions from two or more Observables without interleaving them
 - `Replay` : ensure that all observers see the same sequence of emitted items, even if they subscribe after the Observable has begun emitting items
 - `Merge` : combine multiple Observables into one by merging their emissions

## 用到的开源库：
 * [Fast-Android-Networking](https://github.com/amitshekhariitbhu/Fast-Android-Networking)

## Thanks
 * [重新理解响应式编程](http://www.jianshu.com/p/c95e29854cb1)
 * [关于RxJava最友好的文章](http://www.jianshu.com/p/6fd8640046f1)

## 开源协议
本项目采用 Apache 授权协议，欢迎大家在这个基础上进行改进，并与大家分享。

    Copyright 2017 Ordosbxy(零一空间)

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

